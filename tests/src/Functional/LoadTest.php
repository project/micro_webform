<?php

namespace Drupal\Tests\usine_webform\Functional;

use Drupal\Core\Url;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group usine_webform
 */
class LoadTest extends MicroSiteBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['micro_webform', 'micro_site'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
